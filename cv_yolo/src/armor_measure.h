// name : egg
// Created by rapper2 on
// time : 2021/9/2

#include "yolov5.h"
#include "librealsense2/rs.hpp"
#include "librealsense2/rsutil.h"
//获取深度像素对应长度单位转换
float get_depth_scale(const rs2::device& dev);
//对齐彩色图像
cv::Mat align_Depth2Color(cv::Mat depth,const cv::Mat &color, rs2::pipeline_profile& profile);
//测距
float Measure_distance(cv::Mat &color,cv::Mat depth,cv::Size range,rs2::pipeline_profile& profile);
float armor_distance(std::vector<Roi_output> roi_frame,rs2::pipeline_profile& profile);


