// name : egg
// Created by rapper2 on 2021/8/12.
//
#pragma once//文件在编译的过程中只打开一次
#include "iostream"
#include "cmath"
#include "opencv2/opencv.hpp"
#include "inference_engine.hpp"

struct Output
{
    int id;//类别的id
    float confidence;//置信度
    cv::Rect box;//框
};
struct Roi_output
{
    cv::Mat color_frame;//彩色图数据
    cv::Mat depth_frame;//深度数据
};
struct Dst_output
{
    cv::Mat blue_frame;//蓝色二值化
    cv::Mat red_frame;//红色二值化
};

struct Output_txt
{
    int64 TickStart;
    float distance;
};

class YOLO //yolo的类
{
public:
    bool readModel(cv::dnn::Net &net,std::string &netPath,bool isGPU);//写入网络的地址，不使用GPU加速
    void Detect(cv::Mat &SrcImg ,cv::dnn::Net &net,std::vector<Output> &output);//目标检测vector为分类器，定义动态向量数组
    void drawPred(cv::Mat &img,cv::dnn::Net &net,std::vector<Output> result, Output_txt &result_txt);//scalar是写颜色数据//绘画出框架
    void ROIImg(cv::Mat &color_img,cv::Mat &depth_img,std::vector<Output> result,std::vector<Roi_output> &roi_result,struct ArmorDetectDebug armor_detectDebug);//提取ROI
    void ThresholdImg(std::vector<Roi_output> roi_result,std::vector<Dst_output> &dst_result,struct ArmorDetectDebug armor_detectDebug,struct setPreprocessParm setPreprocess_parm);//二值化图像
private://私有仓库
    float Sigmoid(float x)//归一化函数
    {
        return static_cast<float >(1.f / (1.f + expf(-x)));
    }
    //anchors锚点为静态变量
    const float netAnchors[3][6] = {{ 10.0, 13.0, 16.0, 30.0, 33.0, 23.0 },//80
                                    { 30.0, 61.0, 62.0, 45.0, 59.0, 119.0 },//40
                                    { 116.0, 90.0, 156.0, 198.0, 373.0, 326.0 }};//20
    const float netStride[3] = { 8.0 ,16.0 ,32.0};//次数
    const int netWidth = 640;//网络模型大小
    const int netHeight = 640;
    float nmsThreshold = 0.5;//0.2//0.45
    float boxThreshold = 0.65;//0.05//0.5
    float classThreshold = 0.7;//0.35

    const int frameWidth = 640;
    const int frameHeight = 480;//实际图片大小

    std::vector<std::string> className = {"red armor","blue armor"};
};



