// name : egg
// Created by rapper2 on .
// time : 2021/9/2
#include "yolov5.h"
#include "armor_measure.h"
#include "iostream"
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/highgui/highgui_c.h"


using namespace std;
using namespace cv;

float get_depth_scale(const rs2::device& dev)
{
    //前往摄像头传感器
    for (rs2::sensor & sensor : dev.query_sensors())//使用与，两者发生一个既可
    {
        //检查是否有深度图像
        if(rs2::depth_sensor dpt = sensor.as<rs2::depth_sensor>())//检查是否有深度图
        {
            return dpt.get_depth_scale();//在数组中返回数值
        }
    }
    throw std::runtime_error("Device Error!");//发生错误打印
}

cv::Mat align_Depth2Color(cv::Mat depth,const cv::Mat &color, rs2::pipeline_profile& profile)
{
    //定义数据流深度与图像//auto默认类型rs2::video_stream_profile
    auto depth_stream = profile.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>();//使用auto可以直接定义之前的数据类型
    auto color_stream = profile.get_stream(RS2_STREAM_COLOR).as<rs2::video_stream_profile>();

    //获得内部参数(使用const只能在内部使用，与静态变量相似)
    const auto intrinDepth = depth_stream.get_intrinsics();//只能在函数内使用
    const auto intrinColor = color_stream.get_intrinsics();

    //直接获取从深度相机坐标系到彩色摄像头坐标系的欧拉转换矩阵
    rs2_extrinsics extrinDepth2Color;//声明
    rs2_error *error;
    rs2_get_extrinsics(depth_stream,color_stream,&extrinDepth2Color,&error);

    //平面点定义
    float pd_uv[2],pc_uv[2];//定义数组
    //空间点定义
    float Pdc3[3],Pcc3[3];

    //获得深度像素与现实单位比例
    float depth_scale = get_depth_scale(profile.get_device());
    int y,x;
    //初始化结果
    Mat result = Mat(color.rows,color.cols,CV_16U,Scalar(0));
    //对深度图像处理
    for(int row=0;row<depth.rows;row++)
    {
        for(int col=0;col<depth.cols;col++)
        {
            pd_uv[0] = (float)col;
            pd_uv[1] = (float)row;
            //得到当前的深度数值
            uint16_t depth_value = depth.at<uint16_t>(row,col);
            //换算单位
            float depth_m = (float)depth_value*depth_scale;//换算成米
            //深度图像的像素点转换为坐标下三维点
            rs2_deproject_pixel_to_point(Pdc3,&intrinDepth,pd_uv,depth_m);
            //深度相机坐标系的三维点转化到彩色的坐标系下
            rs2_transform_point_to_point(Pcc3,&extrinDepth2Color,Pdc3);
            //彩色摄像头坐标系下深度三维点映射到二位平面上
            rs2_project_point_to_pixel(pc_uv,&intrinColor,Pcc3);

            //取得映射后的（u,v）
            x = (int )pc_uv[0];//处理后的数据
            y = (int )pc_uv[1];

            x = x < 0 ? 0 : x;
            x = x > depth.cols-1 ? depth.cols-1 : x;
            y = y < 0 ? 0 : y;
            y = y > depth.rows-1 ? depth.rows-1 : y;

            result.at<uint16_t>(y,x)=depth_value;
        }
    }
    return result;//返回与彩色图对齐的图像
}

float Measure_distance(cv::Mat &color,cv::Mat depth,cv::Size range,rs2::pipeline_profile& profile)
{
    //获得深度像素与现实单位比例
    float depth_scale = get_depth_scale(profile.get_device());
    //计算中心点
    cv::Point center(color.cols/2,color.rows/2);
    //定义计算距离的范围
    cv::Rect RectRange(center.x-range.width/2,center.y-range.height/2,range.width,range.height);

    float distance_sum = 0;
    int   effective_pixel = 0;
    for(int y = RectRange.y;y < RectRange.y + RectRange.height;y++)
    {
        for(int x = RectRange.x;x < RectRange.x + RectRange.width;x++)
        {
            //不是0就有位置信息
            if(depth.at<uint16_t>(y,x))//出现位置信息
            {
                distance_sum += depth_scale*(float)depth.at<uint16_t>(y,x);
                effective_pixel++;//取平均值
            }
        }
    }
    float effective_distance;
    if(distance_sum/(float)effective_pixel >= 0)
        effective_distance = distance_sum/(float)effective_pixel;
    else
        effective_distance = 0;
    return effective_distance;
}

//检测装甲板距离
float armor_distance(std::vector<Roi_output> roi_frame,rs2::pipeline_profile& profile)
{
    float effective_distance;
    if(roi_frame.data())//有数据
    {
        for (auto &frame : roi_frame)//(int i = 0;i < roi_frame.size();i ++)
        {
            int w = frame.color_frame.cols / 2;//暂时保持精度
            int h = frame.color_frame.rows / 2;
            Mat armor_result = align_Depth2Color(frame.depth_frame, frame.color_frame, profile);//对齐
            effective_distance = Measure_distance(frame.color_frame,frame.depth_frame,
                                                  Size(w,h),profile);
        }
    }
    else
        effective_distance = -1.f;
    return effective_distance;
}