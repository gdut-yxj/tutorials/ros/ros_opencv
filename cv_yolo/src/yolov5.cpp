// name : egg
// Created by rapper2 on 2021/8/12.

#include "yolov5.h"
#include "armor_measure.h"
#include "setting.h"
#include "iostream"
#include "Preprocessing.h"

using namespace std;
using namespace cv;
using namespace InferenceEngine;


bool YOLO::readModel(cv::dnn::Net &net, std::string &netPath,bool isGPU)
{

    Core ie;
    vector<string> availableDevices = ie.GetAvailableDevices();//得到设备；
    for (auto &Device : availableDevices)
        printf("supported device name : %s\n",Device.c_str());

    try //异常捕获
    {
        net = cv::dnn::readNetFromONNX(netPath);
    }
    catch (const std::exception &)
    {
        return false;//读取onnx文件失败
    }
    if (isGPU)//GPU
    {
        net.setPreferableBackend(cv::dnn::DNN_BACKEND_INFERENCE_ENGINE);
        net.setPreferableTarget(cv::dnn::DNN_TARGET_OPENCL);
    }
    else//cpu
    {
        net.setPreferableBackend(cv::dnn::DNN_BACKEND_INFERENCE_ENGINE);//cv::dnn::DNN_BACKEND_INFERENCE_ENGINE
        net.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);
    }
    return true;
}

void YOLO::Detect(cv::Mat &SrcImg, cv::dnn::Net &net, std::vector<Output> &output)
{
    Mat blob;
    cv::dnn::blobFromImage(SrcImg,blob,1 / 255.0,cv::Size(netWidth,netHeight)//按比例剪裁
    ,Scalar(0,0,0), true, false);//scale factor为分辨率
    net.setInput(blob);
    vector<Mat> netOutputImg;//定义类
    net.forward(netOutputImg,net.getUnconnectedOutLayersNames());

    vector<int> classIds;//结果id数组//int类型
    vector<float> confidence;//对应的置信度
    vector<Rect> boxes;//框//rect为类型

    float ratio_h = (float)SrcImg.rows / (float)netHeight;//输入的数据流高度
    float ratio_w = (float)SrcImg.cols / (float)netWidth;//输入数据流宽度
    int net_width = (int)className.size() + 5;//输出网络是类别加5
    auto* pdata = (float*)netOutputImg[0].data;
    for (int stride = 0;stride < 3;stride ++)//尺度
    {
        int grid_x = (int)(netWidth / netStride[stride]);
        int grid_y = (int)(netHeight / netStride[stride]);
        for (int anchor = 0;anchor < 3 ;anchor ++)//锚点
        {
            const float anchor_w = netAnchors[stride][anchor * 2];
            const float anchor_h = netAnchors[stride][anchor * 2 + 1];
            for (int i = 0;i < grid_y;i++)
            {
                for (int j = 0;j < grid_x;j++)
                {
                    float box_score = Sigmoid(pdata[4]);
                    if(box_score > boxThreshold)
                    {
                        cv::Mat scores(1,className.size(),CV_32FC1,pdata + 5);
                        Point classIdPoint;
                        double max_class_score;
                        cv::minMaxLoc(scores,0,&max_class_score,0,&classIdPoint);//大小对比转换函数
                        max_class_score = Sigmoid((float)max_class_score);
                        if (max_class_score > classThreshold)//
                        {
                            float x = (Sigmoid(pdata[0]) * 2.f -0.5f + j) * netStride[stride];//x
                            float y = (Sigmoid(pdata[1]) * 2.f -0.5f + i) * netStride[stride];//y
                            float w = powf(Sigmoid(pdata[2]) * 2.f,2.f) * anchor_w;//w
                            float h = powf(Sigmoid(pdata[3]) * 2.f,2.f) * anchor_h;//h
                            int left = (x - 0.5 * w) * ratio_w;
                            int top = (y - 0.5 * h) * ratio_h;

                            classIds.push_back(classIdPoint.x);
                            confidence.push_back(max_class_score);
                            boxes.push_back(Rect(left,top,(int)(w * ratio_w),(int)(h * ratio_h)));
                        }
                    }
                    pdata +=net_width;
                }
            }
        }
    }
    //非极大值抑制消除重叠现象
    vector<int> nms_result;//定义数组
    cv::dnn::NMSBoxes(boxes,confidence,classThreshold,nmsThreshold,nms_result);
    for (int idx : nms_result)
    {
        Output result;//输出结果//结构体重命名
        result.id = classIds[idx];
        result.confidence = confidence[idx];//返回置信度
        result.box = boxes[idx];
        output.push_back(result);//输出数据
    }
}

void YOLO::drawPred(cv::Mat &img,cv::dnn::Net &net, std::vector<Output> result, Output_txt &result_txt) //打印数据
{
    for (auto & draw_result : result)//结果的数量
    {
        int left, top;
        left = draw_result.box.x;
        top = draw_result.box.y;

        cv::rectangle(img, draw_result.box, Scalar(135, 206, 235), 2, 8);//画圈
        string label = "armor";//打印文本置信度
        int baseLine;
        Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
        top = max(top, labelSize.height) - 10;//找出最大高度
        putText(img, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 1, Scalar(135, 206, 235), 2);//打印框架
    }
    //检测FPS
    int64 fps = (int64)getTickFrequency() / (getTickCount() - result_txt.TickStart);
    cv::String rs2_fps = cv::format("FPS:%2ld",fps);
    putText(img,rs2_fps,Point(1,20),FONT_HERSHEY_SIMPLEX,0.7,Scalar(0, 0, 255),1);

    //检测时间延时
    vector<double> layersTimes;
    double freq = cv::getTickFrequency()/1000;//帧数
    double t = (double)net.getPerfProfile(layersTimes)/freq;//得到检测时间
    cv::String dt = cv::format("Detect time:%dms",(int)t);
    putText(img,dt,Point(1,40),FONT_HERSHEY_SIMPLEX,0.7,Scalar(0, 0, 255),1);
    cv::String armor_dt = cv::format("Distance:%2.2fm",result_txt.distance);
    putText(img,armor_dt,Point(450,20),FONT_HERSHEY_SIMPLEX,0.7,Scalar(0, 0, 255),1);
}

void YOLO::ROIImg(cv::Mat &color_img,cv::Mat &depth_img,std::vector<Output> result,std::vector<Roi_output> &roi_result,struct ArmorDetectDebug armor_detectDebug)
{
    const char*roi_color = "ROI_Color";
    namedWindow(roi_color,WINDOW_NORMAL);
    Settings settings;

    if (result.data())//ROI感兴趣提取
        for (auto &roi : result)
        {
            VAL_LIMIT(roi.box.width,0,(frameWidth));//防止崩掉线程
            VAL_LIMIT(roi.box.height,0,(frameHeight));
            VAL_LIMIT(roi.box.x,0,(frameWidth - roi.box.width));
            VAL_LIMIT(roi.box.y,0,(frameHeight - roi.box.height));

            Mat roi_img = color_img(Rect(roi.box.x, roi.box.y, roi.box.width, roi.box.height));
            Mat roi_depth = depth_img(Rect(roi.box.x, roi.box.y, roi.box.width, roi.box.height));

            Roi_output output_roi;//输出数据流
            output_roi.color_frame = roi_img;
            output_roi.depth_frame = roi_depth;
            roi_result.push_back(output_roi);//发送数据

            if (armor_detectDebug.show_img)
                imshow(roi_color,roi_img);
            else
                if(-1 != getWindowProperty(roi_color,1))
                    destroyWindow(roi_color);
        }
    else
    {
        if (armor_detectDebug.show_img)
            imshow(roi_color,255);
        else
        if(-1 != getWindowProperty(roi_color,1))
            destroyWindow(roi_color);
    }

}

void YOLO::ThresholdImg(std::vector<Roi_output> roi_result,std::vector<Dst_output> &dst_result,struct ArmorDetectDebug armor_detectDebug,struct setPreprocessParm setPreprocess_parm)
{
    const char*roi_bin = "ROI_Bin";
    namedWindow(roi_bin,WINDOW_NORMAL);

    static bool read_data = false;
    Mat bin_blue;
    AdaptiveThreshold AdThreshold;

    if(roi_result.data())
    {
        for (auto &src_img : roi_result)
        {
            AdThreshold.mulAndIterBlueTre(src_img.color_frame,bin_blue,setPreprocess_parm);

            Dst_output output_dst;
            output_dst.blue_frame = bin_blue;
            dst_result.push_back(output_dst);

            if(armor_detectDebug.show_img)
                imshow(roi_bin,bin_blue);
            else
                if (-1 != getWindowProperty(roi_bin,1))
                    destroyWindow(roi_bin);
        }
    }
    else
    {
        if(armor_detectDebug.show_img)
            imshow(roi_bin,255);
        else
            if (-1 != getWindowProperty(roi_bin,1))
                destroyWindow(roi_bin);
    }
}