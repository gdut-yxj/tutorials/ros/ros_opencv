// name : egg
// Created by rapper2 on .
// 2021/9/5
#include "iostream"
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"

//阀值计算
#define VAL_LIMIT(val,min,max) \
do {                           \
if ((val) <= (min))            \
{                              \
    (val) = (min);             \
}                              \
else if((val) >= (max))        \
{                              \
    (val) = (max);             \
}                              \
}while(0)

class AdaptiveThreshold//自适应二值化(为找发光的灯条)
{
public:
/**/
    void mulAndIterBlueTre(cv::Mat img,cv::Mat &bin,struct setPreprocessParm setPreprocess_parm);//自适应算法
    int avgGrayThreshold(cv::Mat img);//平均灰度化
};