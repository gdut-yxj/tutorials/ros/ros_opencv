// name : egg
// Created by rapper2 on
// 2021/9/5.
#include "Preprocessing.h"
#include "setting.h"

using namespace cv;
using namespace std;

int AdaptiveThreshold::avgGrayThreshold(cv::Mat img)
{
    long sum_gray = 0;//灰度化总和
    for (int j = 0;j < img.rows; j++)//行
    {
        uchar *data = img.ptr<uchar>(j);//data指针对j行的第一个数据
        for (int i = 0;i < img.cols;i ++)//列
        {
            sum_gray += data[i];//将三个通道的数据相加
        }
    }
    return (int)sum_gray / (img.cols * img.rows);//平均值
}

void AdaptiveThreshold::mulAndIterBlueTre(cv::Mat img, cv::Mat &bin,struct setPreprocessParm setPreprocess_parm)
{
    Mat gray_b = Mat::zeros(img.size(),CV_8UC1);
    Mat gray_g = Mat::zeros(img.size(),CV_8UC1);//黑色的数据

    int imgCols = img.cols * 3;//单通道变成RGB三通道
    for (int i = 0; i < img.rows; i++)
    {
        uchar *p_img = img.data + imgCols * i;//得到图像数据以及图片大小
        uchar *p_gray_b = gray_b.data + gray_b.cols * i;
        uchar *p_gray_g = gray_g.data + gray_g.cols * i;

        int jj = 0;
        for (int j = 0;j < imgCols;j += 3)//
        {
            *(p_gray_b + jj) = *(p_img + j) * 0.7 + *(p_img + j + 1) * 0.2 + *(p_img + j + 2) * 0.1;//R*0.7+G*0.2+B*0.1
            *(p_gray_g + jj) = *(p_img + j + 1);//G通道
            jj ++;
        }
    }
    //最大灰度化
    uchar gray_max = gray_b.data[0];
    for (int i = 0; i < gray_b.rows;i ++)
    {
        uchar *p = gray_b.data + i * gray_b.cols;//p存放内容
        for (int j = 0;j < gray_b.cols;j++)
        {
            if (*(p + j) > gray_max)
                gray_max = *(p + j);
        }
    }

    int thre = gray_max * setPreprocess_parm.gray_max_w / 10. + avgGrayThreshold(gray_b) * setPreprocess_parm.gray_avg_w / 1.;//输入blue进去
    VAL_LIMIT(thre,setPreprocess_parm.threshold_min,setPreprocess_parm.threshold_max);//调用值
    cv::threshold(img,bin,thre,255,THRESH_BINARY);
}
