// name : egg
// Created by rapper2 on .
// 2021/9/7

#include "setting.h"
using namespace std;
using namespace cv;

void Settings::settings_debug(const char* win_name,struct ArmorDetectDebug &armor_detectDebug,struct PreprocessParm &preprocess_parm)
{
    cv::createTrackbar("SHOW_IMG",win_name,&armor_detectDebug.show_img,1, nullptr, nullptr);
    cv::createTrackbar("ARMOR_DEBUG",win_name,&armor_detectDebug.debug,1, nullptr, nullptr);
    cv::createTrackbar("Threshold_min",win_name,&preprocess_parm.threshold_min,255, nullptr, nullptr);
    cv::createTrackbar("Threshold_max",win_name,&preprocess_parm.threshold_max,255, nullptr, nullptr);
    cv::createTrackbar("Gray_max_w",win_name,&preprocess_parm.gray_max_w,10, nullptr, nullptr);
    cv::createTrackbar("Gray_avg_w",win_name,&preprocess_parm.gray_avg_w,10, nullptr, nullptr);
    imshow(win_name,255);
}

void Settings::debug_mode(struct ArmorDetectDebug armor_detectDebug, struct PreprocessParm preprocess_parm,
                          struct setPreprocessParm &setPreprocess_parm)
{
    if(armor_detectDebug.debug)
        memcpy(&setPreprocess_parm,&preprocess_parm,sizeof(setPreprocess_parm));
    else
        memcpy(&setPreprocess_parm,&setPreprocess_parm,sizeof(setPreprocess_parm));
}
