// name : egg
// Created by rapper2 on
// 2021/9/7.
#include "iostream"
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"

struct PreprocessParm
{
    int threshold_min;
    int threshold_max;
    int gray_max_w;
    int gray_avg_w;
    PreprocessParm()
    {
        threshold_min = 197;
        threshold_max = 238;
        gray_max_w = 7;
        gray_avg_w = 5;
    }
};

struct ArmorDetectDebug
{
    int debug;
    int show_img;
    ArmorDetectDebug()
    {
        debug = false;
        show_img = true;
    }
};

struct setPreprocessParm
{
    int threshold_min{};
    int threshold_max{};
    int gray_max_w{};
    int gray_avg_w{};
};

class Settings
{
public:

    /**
    * @brief 是否进入调试模式//设置目标预处理参数(二值化)
    * @param 窗口名称
    * @author egg
    * @date 2021.9.7
    */
    static void settings_debug(const char* win_name,struct ArmorDetectDebug &armor_detectDebug,struct PreprocessParm &preprocess_parm);
    void debug_mode(struct ArmorDetectDebug armor_detectDebug,struct PreprocessParm preprocess_parm,struct setPreprocessParm &setPreprocess_parm);
};

