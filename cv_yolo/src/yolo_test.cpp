// name : egg
// Created by rapper2 on 2021/8/12.

#include "yolov5.h"
#include "armor_measure.h"
#include "Preprocessing.h"
#include "setting.h"
#include "iostream"
using namespace std;

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui_c.h"
using namespace cv;
using namespace dnn;

int main()
{
    YOLO yolo;
    Net net;//dnn
    struct ArmorDetectDebug armor_detectDebug;
    struct PreprocessParm preprocess_parm;
    struct setPreprocessParm setPreprocess_parm;
    string model_path = "/home/rapper2/catkin_ws/src/test/cv_yolo/cv_model/armor.onnx";

    if(yolo.readModel(net,model_path, true))
    {
        cout << "read net success !" << endl;
    }
    else
    {
        cout << "read net failed !" << endl;
    }

    const char*armor_img = "armor_detect";
    const char*debug_mode = "DEBUG_MODE";
    namedWindow(armor_img,WINDOW_AUTOSIZE);
    namedWindow(debug_mode,WINDOW_NORMAL);
/*
    vector<Output> result;
    Output_txt output_txt{};
    AdaptiveThreshold AdThreshold;

    string img_path = "/home/rapper2/catkin_ws/src/test/cv_yolo/images/7.png";
    Mat img = imread(img_path);

    const char*ROIImg = "ROI";
    namedWindow(ROIImg,WINDOW_NORMAL);

    yolo.Detect(img, net, result);
    //二次筛选（1.颜色分割 2.比例匹配 3.绘画中线）
    for (auto &i : result)
    {
        Mat dst_img;
        Mat roi_img = img(Rect(i.box.x, i.box.y,
                               i.box.width, i.box.height));
        AdThreshold.mulAndIterBlueTre(roi_img,dst_img,preprocessParm);
        imshow("DST",dst_img);
        imshow("ROI",roi_img);
    }
    //打印框架//
    yolo.drawPred(img,net, result,output_txt);
    vector<double> layersTimes;
    double freq = cv::getTickFrequency()/1000;//帧数
    double t = (double)net.getPerfProfile(layersTimes)/freq;//得到检测时间
    imshow(armor_img,img);
    waitKey();
*/

    rs2::pipeline pipe;
    rs2::config pipe_config;
    pipe_config.enable_stream(RS2_STREAM_DEPTH,640,480,RS2_FORMAT_Z16,30);
    pipe_config.enable_stream(RS2_STREAM_COLOR,640,480,RS2_FORMAT_BGR8,30);//设置读取color通道

    rs2::pipeline_profile profile = pipe.start(pipe_config);

    //调试模式
    Settings::settings_debug(debug_mode,armor_detectDebug,preprocess_parm);
    memcpy(&setPreprocess_parm,&preprocess_parm,sizeof(setPreprocess_parm));
    while(true)//cvGetWindowHandle(armor_img)//去掉Openvino可加
    {
        vector<Output> result;//定义类型
        vector<Roi_output> roi_result;
        vector<Dst_output> dst_result;
        Output_txt output_txt{};
        Settings settings;

        output_txt.TickStart = getTickCount();
        rs2::frameset frameset = pipe.wait_for_frames();//等待帧
        rs2::frame color_frame = frameset.get_color_frame();//得到彩色图
        rs2::frame depth_frame = frameset.get_depth_frame();//取得深度数据

        const int depth_w = depth_frame.as<rs2::video_frame>().get_width();
        const int depth_h = depth_frame.as<rs2::video_frame>().get_height();
        const int color_w = color_frame.as<rs2::video_frame>().get_width();
        const int color_h = color_frame.as<rs2::video_frame>().get_height();

        Mat color_image(Size(color_w,color_h),
                        CV_8UC3,(void*)color_frame.get_data(),Mat::AUTO_STEP);
        Mat depth_image(Size(depth_w, depth_h),
                        CV_16U, (void *) depth_frame.get_data(), Mat::AUTO_STEP);

        //装甲板检测一次筛选
        yolo.Detect(color_image, net, result);

        //二次筛选（1.颜色分割 2.比例匹配 3.绘画中线）
        //ROI（用于测距）
        yolo.ROIImg(color_image,depth_image,result,roi_result,armor_detectDebug);//直接输入全部的深度与彩色图
        //进入调试模式
        settings.debug_mode(armor_detectDebug,preprocess_parm,setPreprocess_parm);
        //二值化提取颜色
        yolo.ThresholdImg(roi_result,dst_result,armor_detectDebug,setPreprocess_parm);
        //颜色分割


        //测距(暂时用ROI，之后使用装甲板两条灯条形成的矩形)
        output_txt.distance = armor_distance(roi_result,profile);
        //打印数据在窗口
        yolo.drawPred(color_image,net, result,output_txt);

        imshow(armor_img, color_image);
        waitKey(1);
    }

    return 0;
}