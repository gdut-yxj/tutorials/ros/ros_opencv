# YOLOV5教程
---
## 一、安装环境
### 1.Anconda环境搭建 for windows
[Anconda](https://www.anaconda.com/products/individual#Downloads)
- 下载相应版本的Anconda，按照官方的教程进行安装即可。
- 注意安装后需要配置系统环境变量，步骤可参考：https://blog.csdn.net/qq_37392932/article/details/81210470
- 打开CMD命令行，创造新的环境。
``
conda create -n yolo python=3.8
``
- 执行
``
conda info -e
``
- 既可以看到刚刚创建的yolo环境
- 执行
``
activate yolo
``
- 切换到虚拟环境中
- 在进行模型训练代码编写时需要用到jupyter notebook，所以在yolo环境下安装
``
conda install jupyter notebook
``
### 2.Openvino部署 for ubuntu
- 参考博客：[Openvino安装与部署](https://blog.csdn.net/shanglianlm/article/details/100802645?ops_request_misc=&request_id=&biz_id=102&utm_term=openvino%E9%83%A8%E7%BD%B2&utm_medium=distribute.pc_search_result.none-task-blog-2)
---
## 二、训练数据集
### 1.Yolov5下载
- 官方地址：https://github.com/ultralytics/yolov5
- 下载源码后，解压，可以找到requirements.txt文件，里面有安装依赖项的包
- 文件夹中包含了train.py文件、export.py文件以及detect.py文件等
- 进入yolo虚拟环境后，运行
``
pip install -r requirements.txt
``
- 会自动安装玩依赖，如果出现安装失败的原理，是因为链接超时！
- 解决办法：
1.打开C:/用户/“你的计算机名”
2.找到.condarc文件，使用文本打开（没有的话自己创建一个）
3.在文件中添加一下内容：
    ```bash
    channels:
   - http://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/noarch
   - http://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/win-64
   - http://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/noarch
   - http://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/win-64

     show_channel_urls: true
    ```
- 下载完必要的环境后，执行
``
python detect.py --weights yolov5s.pt --source data/images --img 640 --conf 0.25
``
- 开始目标检测，完成后在主目录下可以发现一个runs/detetc文件夹，里面有检测后的官方图片
### 2.模型训练
- 模型训练需要使用显卡加速，需要将显卡驱动安装最新版本：https://www.nvidia.cn/Download/index.aspx?lang=cn
- 为了完成训练工作，需要将训练后的图片按照指定的格式进行整理，对待识别的目标进行标注
标注工具为labelimg的软件：https://blog.csdn.net/qq_45701791/article/details/113992622
- 之后需要安装labelimg运行的依赖
    ```bash
    conda install pyqt=5
    conda install -c anaconda lxml
    pyrcc5 -o libs/resources.py resources.qrc
    ```
- 之后在yolo环境中进入labelimg-master文件夹
``
python labelimg.py
``
- 进入界面之后，将View的前4个选强全部勾选上，最后需要把标记模式改为yolo(save下面)
- 软件右上角可以打开自动归类选项
- 最后鼠标拖拽就可以进行标注
- 所有图片标注好后，保存可以看到labels文件夹中有很多txt文件，每个文件都包含了标记的类别和框的位置
- 最后新建yaml文件，文件放在labels与图片文件下，文件内容中train和val都是我们images的目录，labels的目录不用写进去，会自动识别。nc代表识别物体的种类数目，names代表种类名称，如果多个物体种类识别的话，可以自行增加。
```bash
train: ../yolo_A/images/
val: ../yolo_A/images/
# number of classes
nc: 1
# class names
names: ['blue armor']
```
- 完成标注材料之后，开始训练模型
  ``
  python train.py --img 640 --batch 28 --epochs 100 --data ../yolo_A/blue_armor.yaml --weights yolov5s.pt
  ``
  其中batch绝对训练的精度，直接受到GPU性能影响，过大会报错：显存不足，故可以适当调整
  epochs是训练次数，可以根据性能适当调整
- 训练完成后会出现pt权重文件，该文件为训练后的模型
- 可以通过detect文件检测自己训练模型的效果
  ``
  python detect.py --img 640 --conf 0.25 --weights ../runs/weights/best.pt --source data/images
  ``

  ---
## 三、OpenCV开发
### 1.转化模型格式
- 由于OpenCV只能识别ONNX文件，所以需要将pt文件转化为onnx文件，但官方的转换文件不支持slice赋值，无法被OpenCV的DNN读取，故需要对源码进行修改。
  ```python
  class Focus(nn.Module):
     # Focus wh information into c-space
     def __init__(self, c1, c2, k=1, s=1, p=None, g=1, act=True):  # ch_in, ch_out, kernel, stride, padding, groups
         super(Focus, self).__init__()
         self.conv = Conv(c1 * 4, c2, k, s, p, g, act)
         # self.contract = Contract(gain=2)
     def forward(self, x):  # x(b,c,w,h) -> y(b,4c,w/2,h/2)
         # return self.conv(torch.cat([x[..., ::2, ::2], x[..., 1::2, ::2], x[..., ::2, 1::2], x[..., 1::2, 1::2]], 1))
         N, C, H, W = x.size()  # assert (H / s == 0) and (W / s == 0), 'Indivisible gain'
         s = 2
         x = x.view(N, C, H // s, s, W // s, s)  # x(1,64,40,2,40,2)
         x = x.permute(0, 3, 5, 1, 2, 4).contiguous()  # x(1,2,2,64,40,40)
         y = x.view(N, C * s * s, H // s, W // s)  # x(1,256,40,40)
         return self.conv(y)
  ```
  yolov5/models/common.py
  ```python
  class Detect(nn.Module):
    stride = None  # strides computed during build
    export = False  # onnx export
 
    def __init__(self, nc=80, anchors=(), ch=()):  # detection layer
        super(Detect, self).__init__()
        self.nc = nc  # number of classes
        self.no = nc + 5  # number of outputs per anchor
        self.nl = len(anchors)  # number of detection layers
        self.na = len(anchors[0]) // 2  # number of anchors
        self.grid = [torch.zeros(1)] * self.nl  # init grid
        a = torch.tensor(anchors).float().view(self.nl, -1, 2)
        self.register_buffer('anchors', a)  # shape(nl,na,2)
        self.register_buffer('anchor_grid', a.clone().view(self.nl, 1, -1, 1, 1, 2))  # shape(nl,1,na,1,1,2)
        self.m = nn.ModuleList(nn.Conv2d(x, self.no * self.na, 1) for x in ch)  # output conv
 
    def forward(self, x):
        # x = x.copy()  # for profiling
        z = []  # inference output
        self.training |= self.export
        for i in range(self.nl):
            x[i] = self.m[i](x[i])  # conv
            bs, _, ny, nx = x[i].shape  # x(bs,255,20,20) to x(bs,3,20,20,85)
            x[i] = x[i].view(bs, self.na, self.no, ny, nx).permute(0, 1, 3, 4, 2).contiguous()
            x[i] = x[i].view(bs * self.na * ny * nx, self.no).contiguous()
        return torch.cat(x)
        #     if not self.training:  # inference
        #         if self.grid[i].shape[2:4] != x[i].shape[2:4]:
        #             self.grid[i] = self._make_grid(nx, ny).to(x[i].device)
        #
        #         y = x[i].sigmoid()
        #         y[..., 0:2] = (y[..., 0:2] * 2. - 0.5 + self.grid[i]) * self.stride[i]  # xy
        #         y[..., 2:4] = (y[..., 2:4] * 2) ** 2 * self.anchor_grid[i]  # wh
        #         z.append(y.view(bs, -1, self.no))
        #
        # return x if self.training else (torch.cat(z, 1), x)
  ```
  yolov5/models/yolo.py
- 修改完代码之后，进入虚拟环境，进入yolov5-master
  ``
  python export.py --batch 1 --weights ../runs/weights/best.pt
  ``
- 安装netron
  ``pip install netron``
- 查看onnx的结构,进入.onnx所在文件夹
  ```bash
  python
  import netron
  netron.start("best.onnx")
  ```
  ![img1](img/10.png)
### 2.使用DNN模块
- 将支持slice赋值的onnx文件复制到ROS特定路径下，创建pkg依赖包
- 使用OpenCV中DNN加载网络
  ```c++
  try //异常捕获
    {
        net = cv::dnn::readNetFromONNX(netPath);
    }
    catch (const std::exception &)
    {
        return false;//读取onnx文件失败
    }
    if (isGPU)//GPU
    {
        net.setPreferableBackend(cv::dnn::DNN_BACKEND_INFERENCE_ENGINE);
        net.setPreferableTarget(cv::dnn::DNN_TARGET_OPENCL);
    }
    else//cpu
    {
        net.setPreferableBackend(cv::dnn::DNN_BACKEND_INFERENCE_ENGINE);//cv::dnn::DNN_BACKEND_INFERENCE_ENGINE
        net.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);
    }
    return true;
  ```
### 3.目标检测
- 使用DNN网络读取模型数据
  ```c++
  float* pdata = (float*)netOutputImg[0].data;
  ```
- 通过YOLOV5预测的深度信息寻找三个深度的数据，最终得到识别物体的位置
  ```c++
  Mat blob;
    cv::dnn::blobFromImage(SrcImg,blob,1 / 255.0,cv::Size(netWidth,netHeight)//按比例剪裁
    ,Scalar(0,0,0), true, false);//scale factor为分辨率
    net.setInput(blob);
    vector<Mat> netOutputImg;//定义类
    net.forward(netOutputImg,net.getUnconnectedOutLayersNames());

    vector<int> classIds;//结果id数组//int类型
    vector<float> confidence;//对应的置信度
    vector<Rect> boxes;//框//rect为类型

    float ratio_h = (float)SrcImg.rows / netHeight;//输入的数据流高度
    float ratio_w = (float)SrcImg.cols / netWidth;//输入数据流宽度
    int net_width = className.size() + 5;//输出网络是类别加5
    float* pdata = (float*)netOutputImg[0].data;
    for (int stride = 0;stride < 3;stride ++)//尺度
    {
        int grid_x = (int)(netWidth / netStride[stride]);
        int grid_y = (int)(netHeight / netStride[stride]);
        for (int anchor = 0;anchor < 3 ;anchor ++)//锚点
        {
            const float anchor_w = netAnchors[stride][anchor * 2];
            const float anchor_h = netAnchors[stride][anchor * 2 + 1];
            for (int i = 0;i < grid_y;i++)
            {
                for (int j = 0;j < grid_x;j++)
                {
                    float box_score = Sigmoid(pdata[4]);
                    if(box_score > boxThreshold)
                    {
                        cv::Mat scores(1,className.size(),CV_32FC1,pdata + 5);
                        Point classIdPoint;
                        double max_class_score;
                        cv::minMaxLoc(scores,0,&max_class_score,0,&classIdPoint);//大小对比转换函数
                        max_class_score = Sigmoid((float)max_class_score);
                        if (max_class_score > classThreshold)//
                        {
                            float x = (Sigmoid(pdata[0]) * 2.f -0.5f + j) * netStride[stride];//x
                            float y = (Sigmoid(pdata[1]) * 2.f -0.5f + i) * netStride[stride];//y
                            float w = powf(Sigmoid(pdata[2]) * 2.f,2.f) * anchor_w;//w
                            float h = powf(Sigmoid(pdata[3]) * 2.f,2.f) * anchor_h;//h
                            int left = (x - 0.5 * w) * ratio_w;
                            int top = (y - 0.5 * h) * ratio_h;

                            classIds.push_back(classIdPoint.x);
                            confidence.push_back(max_class_score);
                            boxes.push_back(Rect(left,top,(int)(w * ratio_w),(int)(h * ratio_h)));
                        }
                    }
                    pdata +=net_width;
                }
            }
        }
    }
    ```
- 使用nms非极大抑制函数，防止预测框重复
  ```c++
  vector<int> nms_result;//定义数组
    cv::dnn::NMSBoxes(boxes,confidence,classThreshold,nmsThreshold,nms_result);
    for (int i = 0;i < nms_result.size();i++)
    {
        int idx = nms_result[i];
        Output result;//输出结果//结构体重命名
        result.id = classIds[idx];
        result.confidence = confidence[idx];//返回置信度
        result.box = boxes[idx];
        output.push_back(result);//输出数据
    }
  ```
- 最终将目标位置以及置信度打印出来，实现目标检测
  
  ![img2](img/21.png)